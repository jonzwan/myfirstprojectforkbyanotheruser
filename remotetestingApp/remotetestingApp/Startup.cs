﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(remotetestingApp.Startup))]
namespace remotetestingApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
